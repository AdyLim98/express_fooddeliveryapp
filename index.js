const express = require('express')
const mongoose = require('mongoose');

const app = express()
const userController = require('./controller/userController')
const categoryController = require('./controller/categoryController')
const restaurantController = require('./controller/restaurantController')
const orderController = require('./controller/orderController')
// app.use('/',userController)

//Once import the code below/import controller which i had import model into the controller , then import controller also will add table in mongo atlas automatically , mongo atlas there will add table automatically
// const userModel = require('./model/user');
// const categoryModel = require('./model/category');
// const orderModel = require('./model/order');
// const restaurantModel = require('./model/restaurant');

//MUST INSTALL AND INCLUDE "dotenv"
require('dotenv').config()

// to deploy backend to heroku ,heroku create , git add , commit ,git push heroku master
// and heroku cannot read through the code in env ,but we can set the env config (MONGO_URL)
// in heroku website , then it can read after we config it (what I want to say is we no need change the env ,or other code ,just need go to heroku website to make some config by addding MONGO_URL(in env) to heroku website :))

// ORIGINAL
// `mongodb+srv://${username}:${password}@${cluster}.mongodb.net/${dbname}?retryWrites=true&w=majority`, 
// const mongoAtlasUrl = "mongodb+srv://adylim1998:adylim1998@cluster0.ukf6q.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
const mongoAtlasUrl = process.env.MONGODB_URL
console.log("URL:",mongoAtlasUrl)

//To connect to mongo atlas (NECESSARY)
const connectionParams = {
    useNewUrlParser:true,
    // useCreateIndex:true,
    useUnifiedTopology:true,
    // useFindAndModify:false
}

//Mongo Atlas Connection
mongoose.connect(mongoAtlasUrl,connectionParams).then(()=>{
    console.log("Database Connection Successful")
    //code below is necessary also
    // var db = mongoose.connection

    // app.use('/',userController.getUser)
    // app.use('/',userController.signUp)
    
    // app.listen(3000,function(){
    //     console.log("Server is running....")
    // })
}).catch((err)=>{
    console.log("Database Connection Error: ",err)
})

const PORT = process.env.PORT || 3000;


app.use(express.urlencoded({
  extended: true
}));
//bodyParser is add into express already ,so can just write the code as below
//No app.use(bodyParser.json()) ANYMORE for LATEST VERSION EXPRESS
app.use(express.json());

app.get('/',(req,res)=>res.send('Server is running ...'))
app.get('/server',(req,res)=>res.send('Server is running23 ...'))

app.use('/',userController.getAllUser)
app.use('/',userController.signUp)
app.use('/',userController.login)
app.use('/',userController.getUser)

app.use('/',categoryController.addCategory)
app.use('/',categoryController.getCategory)

app.use('/',restaurantController.addRestaurant)
app.use('/',restaurantController.addMenu)
app.use('/',restaurantController.editMenu)
app.use('/',restaurantController.deleteMenu)

app.use('/',restaurantController.createAddOn)
app.use('/',restaurantController.createFoodOptions)
app.use('/',restaurantController.createBeverageOptions)

app.use('/',restaurantController.listRestaurants)
app.use('/',restaurantController.getRestaurant)

app.use('/',orderController.addOrder)
app.use('/',orderController.listOrder)
app.use('/',orderController.updateStatusOrder)
app.use('/',orderController.cancelOrder)


// Test
// ---------------------------------------
app.use('/',restaurantController.test)
// ---------------------------------------

app.listen(PORT,function(){
    console.log("Server is running....")
})