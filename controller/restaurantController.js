const express = require("express")
const router = express.Router()
const resModel = require('../model/restaurant')
const categoryModel = require('../model/category')

exports.addRestaurant = router.post('/create/restaurant',async (req,res)=>{
    var resName = await resModel.find({name:req.body.name})
    var resAddr = await resModel.find({address:req.body.address}) 

    console.log("WTF:",((resName && resAddr).length == 0))
    // if( !((resName && resAddr).length == 0)) return res.send("Restaurant Name with same Address is Existed !!!")
    if( ((resName.length >=1) && (resAddr.length >=1) )) return res.send("Restaurant Name with same Address is Existed !!!")

    //menu 
    // ---------------------------------------------
    
    // cuisineName
    var cuisineName = req.body.cuisineName
    //price
    var price= req.body.price
    //cuisine_image
    var cuisine_image = req.body.cuisine_image
    //ingredients
    var ingredients= req.body.ingredients

    var restaurant = new resModel({
        name : req.body.name,
        rating : req.body.rating,
        description: req.body.description,
        address:req.body.address,
    })
    // var addON_cuisine= req.body.addON_cuisine
    // var addON_price= req.body.addON_price
    // var addON_image= req.body.addON_image
    
    // var addON_item = {
    //     cuisine_image:addON_cuisine,
    //     price:addON_price,
    //     cuisine_name:addON_image
    // }
    // var addON_item = []

    var menu_item = {
        cuisine_image:cuisine_image,
        ingredients:ingredients,
        price:price,
        cuisine_name:cuisineName,
        // addOn:addON_item.push({
        //     cuisine_name:addON_cuisine,
        //     price:addON_price,
        //     cuisine_image:addON_image
        // })
    }
    
    restaurant.menu.push(menu_item)
  
    // -------------------------------------------------------------
    
    var categoryType = req.body.category
    var resImage = req.body.restaurantImage
    
    // categoryType
    // -------------------------------------------------------------
    if(categoryType){
        var category = await categoryModel.find({type:categoryType})
    }else{
        restaurant.categoryId = []
    }
    
    if(category)  restaurant.categoryId.push(category[0])
    
    // resImage
    // -------------------------------------------------------------
    if(resImage){
        restaurant.restaurant_image = resImage
    }else{
        restaurant.restaurant_image = ""
    }
    
    restaurant.save(function(err,result){
        if(err) return res.status(400).send(err)
        res.status(200).send(result)
    })
    
})

exports.createAddOn = router.post('/create/addon/menu/:menu_id/restaurant/:restaurant_id',async(req,res)=>{
    await resModel.updateOne({_id:req.params.restaurant_id,"menu._id":req.params.menu_id},{
        $push:{
            "menu.$.addOn":req.body
        }
    },function(err,result){
        if(err) res.status(400).send(err)
        res.status(200).send(result)
    })
})

exports.createFoodOptions = router.post('/create/food_option/menu/:menu_id/restaurant/:restaurant_id',async(req,res)=>{
    var existFoodOption = await resModel.findOne({"menu.food_options.option":req.body.option})
    if(existFoodOption) return res.status(400).send("This Food Option Name is Existed")
    await resModel.updateOne({_id:req.params.restaurant_id,"menu._id":req.params.menu_id},{
        $push:{
            "menu.$.food_options":req.body,
        }
    },function(err,result){
        if(err) res.status(400).send(err)
        res.status(200).send(result)
    })
})

exports.createBeverageOptions = router.post('/create/beverages_option/menu/:menu_id/restaurant/:restaurant_id',async(req,res)=>{
    await resModel.updateOne({_id:req.params.restaurant_id,"menu._id":req.params.menu_id},{
        $push:{
            "menu.$.beverages_options":req.body
        }
    },function(err,result){
        if(err) res.status(400).send(err)
        res.status(200).send(result)
    })
})

// model.update not only can just update / also can array add (push) / array delete (pull)
//add menu cuisine 
exports.addMenu = router.post('/create/menu/:restaurant_id',async(req,res)=>{
    // cuisineName
    var cuisineName = req.body.cuisineName
    //price
    var price= req.body.price
    //cuisine_image
    var cuisine_image = req.body.cuisine_image
    //ingredients
    var ingredients= req.body.ingredients

    await resModel.findByIdAndUpdate(req.params.restaurant_id,{
        $push:{
            //menu below is exactly same name in the restaurant model "menu" array ,so can use $push
            menu:{
                cuisine_image:cuisine_image,
                ingredients:ingredients,
                price:price,
                cuisine_name:cuisineName
            }
        }}
        ).then((result)=> res.status(200).send(result))
        .catch((err)=>res.status(400).json(err))
      
})

// delete menu cuisine
exports.deleteMenu = router.delete('/delete/:restaurant_id/menu/:menu_id',async(req,res)=>{
    // await resModel.findByIdAndRemove({_id:req.params.restaurant_id,"menu._id":req.params.menu_id},function(err,result){
    //     if(err) throw err
    //     console.log(result)
    //     res.status(200).send(result)
    // })

    await resModel.findByIdAndUpdate(req.params.restaurant_id,{
        $pull:{
            menu:{
                _id:req.params.menu_id
            }
        }
    }).then((result)=>res.status(200).json(result))
    .catch((err)=>console.log(err))
    
})

exports.editRestaurant = router.put('/edit/:restaurant_id',async(req,res)=>{
    
    await resModel.findByIdAndUpdate(req.params.restaurant_id,req.body,function(err,result){
        if(err) throw err
        res.status(200).json(result)
    })
})

exports.editMenu = router.put('/edit/:restaurant_id/menu/:menu_id',async(req,res)=>{
    
    await resModel.updateOne({_id:req.params.restaurant_id,"menu._id":req.params.menu_id},{$set:{
        //$ is necessary for update object in array , menu below is array
        //$ operator identifies an element in an array to update without explicitly specifying the position of the element in the array.
        "menu.$.ingredients":req.body.ingredient,
        "menu.$.cuisine_name":req.body.cuisineName,
        "menu.$.price":req.body.price,
        "menu.$.cuisine_image":req.body.cuisine_image

    }},function(err,result){
        if(err) throw err
        res.status(200).json(result)
    })
})

exports.listRestaurants = router.get('/restaurants',async(req,res)=>{
    await resModel.find({},function(err,result){
        if (err) throw err
        return res.status(200).json(result)
    })
   
    //return all related documents inside the foreign ref with populate method
    // await resModel.find({}).populate("categoryId").exec(function(err,result){
    //     if (err) throw err
    //     return res.status(200).json(result)
    // })
})

exports.getRestaurant = router.get('/restaurant/:id',async(req,res)=>{
    // await resModel.findById(req.params.id).exec(function(err,result){
    //     if (err) throw err
    //     return res.status(200).json(result)
    // })
    
    //Populate method and its params is must same as the attribute schema in model (categoryId)
    await resModel.findById(req.params.id).populate("categoryId").exec(function(err,result){
        if (err) throw err
        console.log("Result:",result)
        console.log("Result-Type:",result.categoryId[0].type)
        return res.status(200).json(result)
    })
})

// Problem
// exports.createAddOn = router.put('/create/restaurant/:restaurant_id/menu/:menu_id',async (req,res)=>{
//     await resModel.updateOne({_id:req.params.restaurant_id,"menu":{"$elemMatch":{"_id":req.params.menu_id}}},
//     {
//         $push:{
//             "menu.$.addOn.$.cuisine_name":req.body.cuisine_name,
//             "menu.$.addOn.$.price":req.body.price,
//             "menu.$.addOn.$.cuisine_image":req.body.cuisine_image,
//         }
//     }).exec(function(err,result){
//         if(err) throw err
//         res.status(200).send(result)
//     })
// })

// ------------------------------
// Test
exports.test = router.get('/test/:restaurant_id/menu/:id',function(req,res){
    // this api is just return the related document object which inside array ("select" , "$elemMatch")
    // code below is used "select" to Look Into the array (menu) ,then use $elemMatch to find which match it id inside the menu
    resModel.findOne({_id:req.params.restaurant_id}).select({menu:{$elemMatch:{_id:req.params.id}}})
    .exec(function(err,result){
        res.status(200).send(result)
    })
    
})