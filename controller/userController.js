const express = require('express');
const userModel = require('../model/user');

const { check , validationResult } = require('express-validator/check')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const auth = require('../middleware/auth')

const router = express.Router()

exports.getAllUser = router.get('/users',(req,res)=>{
    // res.send("Hello World")
    // userModel.find({},function(err,result){
    //     return res.status(200).send(result)
    // })
    // exec method and result is same as code above
    userModel.find({}).exec(function(err,result){
        return res.status(200).send(result)
    })
})

// res.json / res.send 
// cannot res.json.send
// [
//     also cannot like below :
//     res.send("success")
//     res.json(data)
// ] (What i wanna to say is satu biji code ,cannot involve res.send and res.json ,even though they are in next line)

exports.signUp = router.post('/signUp',[check("name","Please Enter a valid name").not().isEmpty(),
                                        check("email","Please Enter a valid email").isEmail(),
                                        check("password","Please Enter a password with minimum 6 ").isLength({
                                            min:6
                                        })], async (req,res) => {
                                    
                                            const errors = validationResult(req);
                                            console.log("Errrors....",errors)
                                            //if error with input requirement ,so trigger below (MUST INCLUDE isEmpty() ,maybe only got this isEmpty() then can check more further ,I had try no isEmpty() [what i type is if(error) then had a bit problem])
                                            if(!errors.isEmpty()){
                                                return res.status(400).json({
                                                    errors:errors.array()
                                                })
                                            }
                                            
                                            try{
                                                //database also need use "await"
                                                let user = await userModel.findOne({
                                                    email:req.body.email
                                                })
                                            
                                                if(user) return res.status(400).json({msg:"Email Already Exists,Please use another email to Sign Up"})
                                                
                                                const newUser = new userModel({
                                                    name : req.body.name,
                                                    email: req.body.email,
                                                    password:req.body.password
                                                })
                                                
                                                const salt = await bcrypt.genSalt(10)
                                                newUser.password = await bcrypt.hash(req.body.password,salt)
                                                console.log("Hash2",newUser.password)
                                                
                                                var jsonToken = jwt.sign({user:{id:newUser._id}},"secret",{
                                                    expiresIn:10000
                                                })

                                                if(jsonToken){
                                                    newUser.token = jsonToken
                                                    console.log("newUser",newUser)
                                                    await newUser.save()
                                                    return res.status(200).json({token:jsonToken})
                                                }
                                            }catch(err){
                                                console.log("3")
                                                console.log("Here:",err.message)
                                                res.status(500).send("Error in Saving")
                                            }
})

exports.login = router.post('/login',[
                                        check("email","Please Enter a valid email").isEmail(),
                                        check("password","Please Enter a valid password").isLength({
                                        min:6
                                    })],async (req,res)=>{
                                        
                                        const errors = validationResult(req);
                                       
                                        // if error with input requirement ,so trigger below
                                        if(!errors.isEmpty()){
                                            return res.status(400).json({
                                                errors:errors.array()
                                            })
                                        }

                                        try{
                                           console.log("Hi2")
                                           var user = await userModel.findOne({
                                                email:req.body.email
                                            })
                                            console.log("UserData:",user)

                                            if(!user) return res.status(400).json({msg:"Email Incorrect"})
                                            
                                            //check password match or not
                                            const isMatch = await bcrypt.compare(req.body.password,user.password)
                                            if(!isMatch) return res.status(400).json({msg:"Password not match"})

                                            var jsonToken = jwt.sign({user:{id:user._id}},"secret",{expiresIn:10000})
                                            // res.status(200).json({jsonToken})
                                            
                                            if(user.email === req.body.email && isMatch){
                                                res.send("Login Successful with Token:" + jsonToken)
                                            }

                                        }catch(error){
                                            console.log("Error:",error)
                                            res.status(500).send("Server Error:"+ error)
                                        }

})

//This api is created to get userInfo by Key In the token (headers) More secure
//auth middleware below as a "parameter" and need to define in other file.js
exports.getUser = router.get('/user',auth,async(req,res)=>{
    try{
        // request.user is getting fetched from Middleware after "token authentication"
        const user = await userModel.findById(req.user.id)
        res.json(user)
    }catch(error){
        res.send({message:"Error in Fetching User Personal Data : "+error})
    }
})

//Write by myself
// exports.signUp = router.post('/signup',(req,res)=>{
//     if(!req.body) res.send("Pls input some data!!!")
//     // const {name,email,password} = req.body
//     const user = new userModel({
//         name:req.body.name,
//         email:req.body.email,
//         password:req.body.password
//     })
//     user.save(function(err,result){
//         if(err) return res.status(404).send(err)
//         return res.status(200).send("New user is created :" , result)
//     })

// })


// module.exports = router;
