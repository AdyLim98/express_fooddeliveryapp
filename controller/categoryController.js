const express = require('express')
const router = express.Router()
const categoryModel = require('../model/category')

const { check , validationResult } = require('express-validator/check')

exports.addCategory = router.post('/create/category',[check("type","Please Key In the Category Type").not().isEmpty()], (req,res) => {
    
    const errors = validationResult(req);
    if(!errors.isEmpty()) return res.status(400).json({
        errors:errors.array()
    })
    
    const {type} = req.body
    if(!req.body) res.send("Type is required !!")
    
    try{
        var category = new categoryModel({
            type:type
        })
        category.save()
        console.log("hi")
        res.status(200).send("'"+type+"' Category Added!!")

    }catch(err){
        console.log("Err:",err)
    }
})

exports.getCategory = router.get('/category',async(req,res)=>{
    try{
        await categoryModel.find({}).sort({createdAt:1}).exec(function(err,result){
            res.status(200).send(result)
        })
    }catch(err){
        console.log(err)
    }
})