var express = require('express')
var router = express.Router()
var orderModel = require('../model/order')
var userModel = require('../model/user')
var resModel = require('../model/restaurant')

exports.addOrder = router.post('/user/:user_id/create/order/:restaurant_id',async(req,res)=>{
    var quantity = req.body.quantity
    var item_name = req.body.item_name

    var order = new orderModel({
        total_amount:req.body.total_amount,
        orderStatus:req.body.status,
    })

    var item = {
        quantity:quantity,
        name:item_name
    }

    var user = await userModel.findById(req.params.user_id)
    // console.log(user._id)

    var restaurant = await resModel.findById(req.params.restaurant_id)

    order.userID = user._id
    order.restaurantID = restaurant._id

    order.item.push(item)

    order.save(function(err,result){
        if(err) throw err
        res.status(200).json(result)
    })
})

exports.listOrder = router.get('/orders',async(req,res)=>{
    await orderModel.find({}).populate("userID").populate("restaurantID").exec(function(err,result){
        if(err) throw err
        res.status(200).send(result)
    })
})

exports.updateStatusOrder = router.put('/order/:order_id',async(req,res)=>{
    await orderModel.findByIdAndUpdate(req.params.order_id,{$set:{orderStatus:req.body.status}},function(err,result){
        if (err) throw err
        res.status(200).send(result)
    })
})

exports.cancelOrder = router.delete('/order/:order_id',async(req,res)=>{
    await orderModel.findByIdAndRemove(req.params.order_id,function(err,result){
        if (err) throw err
        res.status(200).send(result)
    })
})