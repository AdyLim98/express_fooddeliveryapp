<h1>Express_FoodDelivery_APP</h1>
<p>This project is build with Mongoose + Express , connect and store data into the Mongodb Atlas ,deploy with herokuapp</p>
<p>This project is just a simple backend API only </p>
<p>API Link -> https://serene-meadow-59286.herokuapp.com/ </p>

<h1>[User Api]</h1>
<p><b>Get All User</b></p>
<p>/users [GET]</p>
<p><b>Get User (Auth) who Login</b></p>
<p>/user [header("token")] [GET]</p>
<p><b>Sign Up - User</b></p>
<p>/signUp [POST]</p>
<p>name,email,password</p>
<p><b>Login - User</b></p>
<p>/login [POST]</p>
<p>email,password</p>
<br>

<p><h1>[Category Api]</h1></p>
<p><b>Get All Category</b></p>
<p>/category [GET]</p>
<b>Create Category</b>
<p>/create/category [POST]</p>
<p>type</p>
<br>

<p><h1>[Order Api]</h1></p>
<b>List All Order</b>
<p>/orders [GET]</p>
<b>Create Order</b>
<p>/user/:user_id/create/order/:restaurant_id [POST]</p>
<p>quantity,item_name,total_amount,status</p>
<b>Update Status Order</b>
<p>/order/:order_id [PUT]</p>
<p>status</p>
<b>Cancel Order</b>
<p>/order/:order_id [DELETE]</p>
<br>

<p><h1>[Restaurant Api]</h1></p>
<b>Add Restaurant</b>
<p>/create/restaurant [POST]</p>
<p>name,address,cuisineName,price,cuisine_image,ingredients,category,restaurantImage</p>
<b>Add Menu</b>
<p>/create/menu/:restaurant_id [POST]</p>
<p>cuisineName,price,cuisine_image,ingredients</p>
<b>AddOn</b>
<p>/create/addon/menu/:menu_id/restaurant/:restaurant_id [POST]</p>
<p>cuisine_name,price,cuisine_image</p>
<b>food_options</b>
<p>/create/food_option/menu/:menu_id/restaurant/:restaurant_id [POST]</p>
<p>option,price</p>
<b>food_options</b>
<p>/create/beverages_option/menu/:menu_id/restaurant/:restaurant_id [POST]</p>
<p>option,price</p>
<b>Delete Menu</b>
<p>/delete/:restaurant_id/menu/:menu_id [DELETE]</p>
<b>Edit Restaurant</b>
<p>/edit/:restaurant_id [PUT]</p>
<p>name,address,cuisineName,price,cuisine_image,ingredients,category,restaurantImage</p>
<b>Edit Menu</b>
<p>/edit/:restaurant_id/menu/:menu_id [PUT]</p>
<p>ingredient,cuisineName,price,cuisine_image</p>
<b>List Restaurants</b>
<p>/restaurants [GET]</p>
<b>GET Restaurant</b>
<p>/restaurant/:id [GET]</p>

<p><b>Test - Just Show the related document inside array only</b></p>
<p>/test/:restaurant_id/menu/:id [GET]</p>




