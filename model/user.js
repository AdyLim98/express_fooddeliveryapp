const mongoose = require('mongoose')
const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required:[true,"Name field is required"],
        trim:true
    },
    email:{
        type:String,
        required:[true,"Email field is required"],
        unique:true
    },
    password:{
        type:String,
        required:[true,"Password field is required"],
        min:6
    },
    token:{
        type:String
    },
    resetPassword:{
        type:String,
        min:6
    },

    address:{
        type:String
    },

    profile_image:{
        type:String
    },
    
    //even though we didnt hard code the fake data of createdAt and updatedAt ,but if we write schema as data structure(createdAt,updatedAt) here,it will add the data by itself(since createdAt and updatedAt had default with DATE.now())
    createdAt:{
        type:Date,
        default:Date.now()
    },
    updatedAt:{
        type:Date,
        default:Date.now()
    }
} 
// ,{timestamps: true}
)

// timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }

// user collection exist in mongo atlas
// const User = mongoose.model("User",userSchema,'user')

//if no specify 'user' in 3 as parameter , then when run npm start ,mongo atlas will give u 'users' collection name automatically
const User = mongoose.model("User",userSchema,"users")
module.exports = User;