const mongoose = require("mongoose")
const categorySchema = new mongoose.Schema({
    type:{
        type:String,
        required:[true,"Type is required"],
        trim:true,
        unique:true
    }
},{timestamps:true})

const category = mongoose.model("Category",categorySchema,'categories')
module.exports = category