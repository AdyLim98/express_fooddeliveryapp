const mongoose = require('mongoose')
const resSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true,
        trim:true
    },
    categoryId:[{
        type:mongoose.Schema.Types.ObjectId,
        //ref must same as the name in name defined in mongoose model (Category)
        ref:"Category"
    }],
    rating:Number,
    description:String,
    address:String,
    restaurant_image:String,
    menu:[
        {
            cuisine_name:String,
            ingredients:String,
            price:Number,
            cuisine_image:String,
            addOn:[{
                cuisine_name:String,
                price:Number,
                cuisine_image:String
            }],
            food_options:[
                {
                    option:String,
                    price:Number
                }
            ],
            beverages_options:[
                {
                    option:String,
                    price:Number
                }
            ]
        }
    ]
},{timestamps:true})

// timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
const Restaurant = mongoose.model("Restaurant",resSchema,"restaurants")
module.exports = Restaurant;