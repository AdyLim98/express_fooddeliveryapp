const mongoose = require('mongoose')
const orderSchema = new mongoose.Schema({
    total_amount:{
        type:Number
    },
    orderStatus:{
        type:Number,
        // validate : {
        //     validator : Number.isInteger,
        //     message   : '{VALUE} is not an integer value'
        // }
    },
    orderDate:{
        type:Date,
        default:Date.now()
    },
    userID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"User"
    },
    item:[
        {
            quantity:{
                type:Number
            },
            name:{
                type:String
            }
        }
    ],
    restaurantID:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"Restaurant"
    }
})

const Order = mongoose.model("Order",orderSchema,'orders')
module.exports = Order